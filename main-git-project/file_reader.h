#ifndef FILE_READER_H
#define FILE_READER_H

#include "sessia_subscription.h"

void read(const char* file_name, sessia_subscription* array[], int& size);

#endif

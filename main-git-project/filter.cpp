#include "filter.h"
#include <cstring>
#include <iostream>

sessia_subscription** filter(sessia_subscription* array[], int size, bool (*check)(sessia_subscription* element), int& result_size)
{
sessia_subscription** result = new sessia_subscription*[size];
result_size = 0;
for (int i = 0; i < size; i++)
{
if (check(array[i]))
{
result[result_size++] = array[i];
}
}
return result;
}

bool check_sessia_subscription_by_historia(sessia_subscription* element)
{
return strcmp(element->discipline, "") == 0 &&
       strcmp(element->student.first_name, "") == 0 &&
       strcmp(element->student.middle_name, "") == 0 &&
       strcmp(element->student.last_name, "") == 0 &&
strcmp(element->mark, "") == 0;
}

bool check_book_subscription_by_7(sessia_subscription* element)
{
return element->mark >= 7;
}
